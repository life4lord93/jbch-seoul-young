module.exports = {
    env: {
        browser: true,
        es2021: true,
    },
    extends: [
        'plugin:prettier/recommended',
        'react-app',
        'airbnb',
        'eslint-config-prettier',
    ],
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 12,
        sourceType: 'module',
    },
    plugins: ['prettier', 'react'],
    rules: {
        'prettier/prettier': 'error',
        indent: ['error', 4],
        'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
        'no-unused-vars': 1,
    },
};
