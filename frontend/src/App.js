import React, { Suspense } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Container } from '@material-ui/core';
import Header from './component/menu/Header';
import EcclesiaList from './component/ecclesia/EcclesiaList';
import EcclesiaForm from './component/ecclesia/EcclesiaForm';

function App() {
    return (
        <>
            <BrowserRouter>
                <Header />
                <Container maxWidth="lg">
                    <Suspense fallback={<div />}>
                        <Switch>
                            <Route
                                exact
                                path="/ecclesia/list"
                                component={EcclesiaList}
                            />

                            <Route
                                exact
                                path="/ecclesia/form"
                                component={EcclesiaForm}
                            />
                        </Switch>
                    </Suspense>
                </Container>
            </BrowserRouter>
        </>
    );
}

export default App;
