import axios from 'axios';
import http from 'http';
import https from 'https';

const noCacheAxios = axios.create({
    headers: {
        Pragma: 'no-cache',
        'Cache-Control': 'no-cache',
        Expires: '0',
        Vary: '*',
    },
    httpAgent: new http.Agent({ keepAlive: true }),
    httpsAgent: new https.Agent({ keepAlive: true }),
});

// eslint-disable-next-line import/prefer-default-export
export { noCacheAxios };
