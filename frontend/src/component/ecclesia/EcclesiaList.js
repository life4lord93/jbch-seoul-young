import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Box, Button, makeStyles } from '@material-ui/core';
import { DataGrid } from '@material-ui/data-grid';
import { noCacheAxios } from '../../util/noCacheAxios';

const ECCLESIA_TYPE = {
    BROTHER: '형제',
    SISTER: '자매',
};

const columns = [
    { field: 'no', headerName: 'ID', width: 100 },
    { field: 'name', headerName: '이름', width: 200 },
    {
        field: 'ecclesiaType',
        headerName: '형제/자매',
        width: 200,
        valueFormatter: ({ value }) => ECCLESIA_TYPE[value],
    },
    {
        field: 'department',
        headerName: '부서',
        width: 200,
        valueFormatter: ({ value }) => value.name,
    },
    {
        field: 'salvationDate',
        headerName: '구원일',
        width: 200,
    },
];

const useStyles = makeStyles({
    datagrid: {
        '&.MuiDataGrid-root .MuiDataGrid-cell:focus': {
            outline: 'none',
        },
    },
});

export default function EcclesiaList() {
    const history = useHistory();
    const [ecclesiaList, setEcclesiaList] = useState([]);

    const [page, setPage] = useState(0);
    const [pageSize, setPageSize] = useState(10);
    const [totalSize, setTotalSize] = useState(0);

    useEffect(() => {
        Promise.resolve()
            .then(() =>
                noCacheAxios.get(
                    `/api/ecclesia/list?page=${page}&pageSize=${pageSize}`
                )
            )
            .then(({ data }) => {
                console.log(data);
                setEcclesiaList([...ecclesiaList, ...data.content]);
                setTotalSize(data.totalElements);
            });
    }, [page]);

    const classes = useStyles();

    const onChangePage = (pageInfo) => {
        setPage(pageInfo.page);
    };

    const onChangePageSize = () => {
        setPageSize(10);
    };

    const handleNewEcclesia = () => {
        history.push('/ecclesia/form');
    };

    return (
        <>
            <Box align="right">
                <Button
                    variant="contained"
                    color="primary"
                    onClick={handleNewEcclesia}
                >
                    추가
                </Button>
            </Box>
            <DataGrid
                getRowId={(row) => row.no}
                columns={columns}
                rows={ecclesiaList}
                pageSize={pageSize}
                page={page}
                rowCount={totalSize}
                className={classes.datagrid}
                autoHeight
                hideFooterSelectedRowCount
                onPageChange={onChangePage}
                onPageSizeChange={onChangePageSize}
            />
        </>
    );
}
