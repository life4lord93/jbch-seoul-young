import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import {
    Box,
    Button,
    FormControl,
    FormHelperText,
    makeStyles,
    NativeSelect,
    Paper,
    Select,
    TextField,
} from '@material-ui/core';
import { noCacheAxios } from '../../util/noCacheAxios';

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(2),
        margin: 'auto',
        width: '400',
    },
}));

const ECCLESIA_TYPE = {
    BROTHER: '형제',
    SISTER: '자매',
};

function initialEcclesia() {
    return {
        name: '',
        salvationDate: '',
        address: '',
        contact: '',
        department: '',
        ecclesiaType: '',
    };
}

export default function EcclesiaForm() {
    const classes = useStyles();
    const history = useHistory();

    const [ecclesia, setEcclesia] = useState(initialEcclesia());
    const [departmentList, setDepartmentList] = useState([]);

    useEffect(() => {
        Promise.resolve()
            .then(() => noCacheAxios.get(`/api/department/list`))
            .then(({ data }) => {
                setDepartmentList([...data]);
            });
    }, []);

    const onChangeInput = (evt) => {
        console.log(evt);
        setEcclesia({
            ...ecclesia,
            [evt.target.name]: evt.target.value,
        });
    };

    const onSave = () => {
        Promise.resolve()
            .then(() => noCacheAxios.post(`/api/ecclesia/`, ecclesia))
            .then(({ data }) => {
                history.push('/ecclesia/list');
            });
    };

    const onCancel = () => {
        history.push('/ecclesia/list');
    };

    return (
        <>
            <Paper className={classes.paper}>
                <FormControl>
                    <TextField
                        name="name"
                        helperText="이름"
                        onChange={onChangeInput}
                        value={ecclesia.name}
                    />
                    <FormControl>
                        <NativeSelect
                            name="ecclesiaType"
                            onChange={onChangeInput}
                            value={ecclesia.ecclesiaType}
                        >
                            <option value="">선택</option>
                            {Object.keys(ECCLESIA_TYPE).map((type) => (
                                <option value={type}>
                                    {ECCLESIA_TYPE[type]}
                                </option>
                            ))}
                        </NativeSelect>
                        <FormHelperText>형제/자매</FormHelperText>
                    </FormControl>
                    <TextField
                        type="date"
                        name="salvationDate"
                        helperText="구원일"
                        value={ecclesia.salvationDate}
                        onChange={onChangeInput}
                    />
                    <TextField
                        name="address"
                        helperText="주소"
                        value={ecclesia.address}
                        onChange={onChangeInput}
                    />
                    <TextField
                        name="contact"
                        helperText="연락처"
                        value={ecclesia.contact}
                        onChange={onChangeInput}
                    />
                    <FormControl>
                        <NativeSelect
                            name="department"
                            value={ecclesia.department}
                            onChange={onChangeInput}
                        >
                            <option value="">선택</option>
                            {departmentList.map((department) => (
                                <option value={department.no}>
                                    {department.name}
                                </option>
                            ))}
                        </NativeSelect>
                        <FormHelperText>부서</FormHelperText>
                    </FormControl>
                </FormControl>
                <Box align="right">
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={onSave}
                    >
                        저장
                    </Button>
                    <Button
                        variant="contained"
                        color="secondary"
                        onClick={onCancel}
                    >
                        취소
                    </Button>
                </Box>
            </Paper>
        </>
    );
}
