import React from 'react';
import { AppBar, IconButton, Toolbar } from '@material-ui/core';
import SideMenu from './SideMenu';

export default function Header() {
    return (
        <>
            <AppBar position="static">
                <Toolbar>
                    <SideMenu />
                </Toolbar>
            </AppBar>
        </>
    );
}
