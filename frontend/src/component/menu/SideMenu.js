import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import {
    Drawer,
    IconButton,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    makeStyles,
} from '@material-ui/core';
import { Face, Inbox, Menu } from '@material-ui/icons';

const useStyles = makeStyles({
    list: {
        width: 250,
    },
});

export default function SideMenu() {
    const classes = useStyles();
    const history = useHistory();
    const [open, setOpen] = useState(false);

    const toggleSideMenu = (state) => (evt) => {
        setOpen(state);
    };

    const movePage = (page) => () => {
        history.push(`${page}`);
    };

    return (
        <>
            <IconButton edge="start" color="inherit" aria-label="menu">
                <IconButton onClick={toggleSideMenu(true)}>
                    <Menu />
                </IconButton>
                <Drawer
                    anchor="left"
                    open={open}
                    onClose={toggleSideMenu(false)}
                >
                    <List>
                        <ListItem
                            button
                            className={classes.list}
                            onClick={movePage('/ecclesia/list')}
                        >
                            <ListItemIcon>
                                <Face />
                            </ListItemIcon>
                            <ListItemText primary="형제/자매 관리" />
                        </ListItem>
                    </List>
                </Drawer>
            </IconButton>
        </>
    );
}
