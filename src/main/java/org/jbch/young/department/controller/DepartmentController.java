package org.jbch.young.department.controller;

import java.util.List;
import lombok.AllArgsConstructor;
import org.jbch.young.department.dto.DepartmentDto;
import org.jbch.young.department.service.DepartmentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/department")
@AllArgsConstructor
public class DepartmentController {

    private final DepartmentService departmentService;

    @GetMapping("/list")
    public List<DepartmentDto> getDepartmentList() {
        return departmentService.getDepartmentList();
    }

}
