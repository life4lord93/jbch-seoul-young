package org.jbch.young.department.service;

import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.jbch.young.department.dto.DepartmentDto;
import org.jbch.young.department.repository.DepartmentRepository;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class DepartmentService {

    private final DepartmentRepository departmentRepository;

    public List<DepartmentDto> getDepartmentList() {
        return departmentRepository.findAll()
            .stream()
            .map(DepartmentDto::of)
            .collect(Collectors.toList());
    }

    public DepartmentDto getDepartment(int no) {
        return DepartmentDto.of(departmentRepository.findById(no)
            .get());
    }
}
