package org.jbch.young.department.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jbch.young.department.entity.Department;
import org.springframework.beans.BeanUtils;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DepartmentDto {

    private Integer no;
    private String name;

    public static DepartmentDto of(Department department) {
        DepartmentDto departmentDto = new DepartmentDto();
        BeanUtils.copyProperties(department, departmentDto);

        return departmentDto;
    }

    private DepartmentDto(int no) {
        this.no = no;
    }

    public static DepartmentDto of(String name) {
        return new DepartmentDto(null, name);
    }

    public static DepartmentDto of(int no, String name) {
        return new DepartmentDto(no, name);
    }

    public Department toEntity() {
        return Department.builder()
            .no(this.no)
            .name(this.name)
            .build();
    }
}
