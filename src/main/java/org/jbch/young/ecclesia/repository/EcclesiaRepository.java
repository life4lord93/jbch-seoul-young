package org.jbch.young.ecclesia.repository;

import org.jbch.young.ecclesia.entity.Ecclesia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EcclesiaRepository extends JpaRepository<Ecclesia, Integer> {

}
