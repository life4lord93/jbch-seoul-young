package org.jbch.young.ecclesia.service;

import javax.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.jbch.young.ecclesia.dto.EcclesiaDto;
import org.jbch.young.ecclesia.repository.EcclesiaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class EcclesiaService {

    private final EcclesiaRepository ecclesiaRepository;

    @Transactional
    public Page<EcclesiaDto> getEcclesiaList(Pageable pageable) {
        return ecclesiaRepository.findAll(pageable)
            .map(EcclesiaDto::of);
    }

    @Transactional
    public EcclesiaDto getEcclesia(int no) {
        return EcclesiaDto.of(ecclesiaRepository.findById(no).get());
    }

    @Transactional
    public void saveEcclesia(EcclesiaDto ecclesia) {
        ecclesiaRepository.save(ecclesia.toEntity());
    }

}
