package org.jbch.young.ecclesia.controller;

import lombok.AllArgsConstructor;
import org.jbch.young.ecclesia.dto.EcclesiaDto;
import org.jbch.young.ecclesia.service.EcclesiaService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/ecclesia")
@AllArgsConstructor
public class EcclesiaController {

    private final EcclesiaService ecclesiaService;

    @GetMapping("/list")
    public ResponseEntity<Page<EcclesiaDto>> getEcclesiaList(
        @RequestParam(defaultValue = "1") int page,
        @RequestParam(defaultValue = "10") int pageSize) {
        return ResponseEntity
            .ok(ecclesiaService.getEcclesiaList(Pageable.ofSize(pageSize).withPage(page)));
    }

    @GetMapping("/{ecclesiaNo}")
    public ResponseEntity<EcclesiaDto> getEcclesia(@PathVariable int ecclesiaNo) {
        return ResponseEntity.ok(ecclesiaService.getEcclesia(ecclesiaNo));
    }

    @PostMapping("/")
    public ResponseEntity<?> saveEcclesia(@RequestBody EcclesiaDto ecclesia) {
        ecclesiaService.saveEcclesia(ecclesia);
        return ResponseEntity.ok().build();
    }
}
