package org.jbch.young.ecclesia.dto;

import java.time.LocalDate;
import lombok.Getter;
import lombok.Setter;
import org.jbch.young.department.dto.DepartmentDto;
import org.jbch.young.ecclesia.entity.Ecclesia;
import org.jbch.young.ecclesia.model.EcclesiaType;
import org.springframework.beans.BeanUtils;

@Getter
@Setter
public class EcclesiaDto {

    private Integer no;
    private String name;
    private LocalDate salvationDate;
    private String address;
    private String contact;
    private DepartmentDto department;
    private EcclesiaType ecclesiaType;

    public static EcclesiaDto of(Ecclesia ecclesia) {
        EcclesiaDto ecclesiaDto = new EcclesiaDto();
        BeanUtils.copyProperties(ecclesia, ecclesiaDto);
        ecclesiaDto.setDepartment(DepartmentDto.of(ecclesia.getDepartment()));
        return ecclesiaDto;
    }

    public Ecclesia toEntity() {
        return Ecclesia.builder()
            .no(this.no)
            .name(this.name)
            .salvationDate(this.salvationDate)
            .address(this.address)
            .contact(this.contact)
            .department(this.department.toEntity())
            .ecclesiaType(this.ecclesiaType)
            .build();
    }
}
