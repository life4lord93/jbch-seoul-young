package org.jbch.young.ecclesia.entity;

import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jbch.young.department.entity.Department;
import org.jbch.young.ecclesia.model.EcclesiaType;

@Entity
@Getter
@Builder
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Ecclesia {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer no;
    private String name;
    private LocalDate salvationDate;
    private String address;
    private String contact;
    @ManyToOne(fetch = FetchType.LAZY)
    private Department department;
    private EcclesiaType ecclesiaType;

}
