package org.jbch.young.ecclesia.model;

public enum EcclesiaType {
    BROTHER, SISTER
}
